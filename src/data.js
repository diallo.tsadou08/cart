//list de données utilisés pour le shop
const list = [
  {
    id: 1,
    title: "Ma belle voiture rouge",
    author: "Aby",
    price: 100,
    img: "https://images.unsplash.com/photo-1493238792000-8113da705763?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8NXx8Y2FyfGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=800&q=60",
    amount: 1,
  },
  {
    id: 2,
    title: "AirPods",
    author: "Aby",
    price: 200,
    img: "https://images.unsplash.com/photo-1600294037681-c80b4cb5b434?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8M3x8YWlycG9kc3xlbnwwfHwwfHw%3D&auto=format&fit=crop&w=800&q=60",
    amount: 1,
  },
  {
    id: 3,
    title: "tasse",
    author: "Aby",
    price: 20,
    img: "https://images.unsplash.com/photo-1458819757519-7581bade511d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8dGFzc2V8ZW58MHx8MHx8&auto=format&fit=crop&w=800&q=60",
    amount: 1,
  },
  {
    id: 4,
    title: "robe",
    author: "Aby",
    price: 60,
    img: "https://images.unsplash.com/photo-1589451359791-f9c33af92239?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Nnx8cm9iZXxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=800&q=60",
    amount: 1,
  },
  {
    id: 5,
    title: "box",
    author: "Aby",
    price: 15,
    img: "https://images.unsplash.com/photo-1573376670774-4427757f7963?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8M3x8Ym94fGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=800&q=60",
    amount: 1,
  },
];

export default list;
